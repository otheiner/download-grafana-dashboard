#!/bin/bash

print_help () {
  echo "The first argument specifies what should be downloaded:" 
  echo "  -for downloading plots from EHN1 use 'EHN1'"
  echo "  -for downloading plots from TI12, use 'TI12'"
  echo "  -for downloading plots both from TI12 and EHN1, use 'both'"
  echo "Usage: ${0} <EHN1/TI12/both> <start date> <end date>"
  echo "Date is in format YYYY-MM-DD hh:mm:ss. Time is in UTC."
  echo "Example: ${0} both 2022-05-04 12:00:00 2022-05-06 12:00:00"
  exit
}

#if [ $# -ne 5 ] || [ $# -ne 6 ]; then
#  print_help
#fi

FROM=`date --date=${2}' '${3}' +0000' +%s`
TO=`date --date=${4}' '${5}' +0000' +%s`
DIR='./grafana_'${FROM}'_'${TO}
DATE_FROM=${2}
DATE_TO=${4}
PRESENTATION='slides_TI12_'${FROM}'_'${TO}

download_TI12 () {
  D1='https://faser-grafana.web.cern.ch/render/d-solo/VT7LpZwGk/tracker-hv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D2='https://faser-grafana.web.cern.ch/render/d-solo/VT7LpZwGk/tracker-hv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D3='https://faser-grafana.web.cern.ch/render/d-solo/-_H_TWQGk/tracker-lv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D4='https://faser-grafana.web.cern.ch/render/d-solo/-_H_TWQGk/tracker-lv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=9&width=1000&height=500&tz=Europe%2FBerlin'
  D5='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D6='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D7='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D8='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FBerlin'
  D9='https://faser-grafana.web.cern.ch/render/d-solo/2s1I9swGk/trb-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D10='https://faser-grafana.web.cern.ch/render/d-solo/o6rynWwGk/scintillators-oracle?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D11='https://faser-grafana.web.cern.ch/render/d-solo/o6rynWwGk/scintillators-oracle?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FBerlin'
  D12='https://faser-grafana.web.cern.ch/render/d-solo/Mc0YFywMk/pdu-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FBerlin'
  D13='https://faser-grafana.web.cern.ch/render/d-solo/Mc0YFywMk/pdu-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D14='https://faser-grafana.web.cern.ch/render/d-solo/fmYHgs9Gz/rack-environment?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D15='https://faser-grafana.web.cern.ch/render/d-solo/fmYHgs9Gz/rack-environment?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FBerlin'
  D16='https://faser-grafana.web.cern.ch/render/d-solo/fmYHgs9Gz/rack-environment?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=9&width=1000&height=500&tz=Europe%2FBerlin'
  D17='https://faser-grafana.web.cern.ch/render/d-solo/dvWV9CuGz/lhc-timing?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D18='https://faser-grafana.web.cern.ch/render/d-solo/o7tUQ7w7z/data-transfer-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D19='https://faser-grafana.web.cern.ch/render/d-solo/o7tUQ7w7z/data-transfer-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D20='https://faser-grafana.web.cern.ch/render/d-solo/o7tUQ7w7z/data-transfer-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=8&width=1000&height=500&tz=Europe%2FBerlin'
  D21='https://faser-grafana.web.cern.ch/render/d-solo/o7tUQ7w7z/data-transfer-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FBerlin'
  D22='https://faser-grafana.web.cern.ch/render/d-solo/S2_8HI57k/fasernu?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D23='https://faser-grafana.web.cern.ch/render/d-solo/FOSuFEf4z/pmt-signal-timing?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D24='https://faser-grafana.web.cern.ch/render/d-solo/B84-cEBVz/late-trigger-fractions?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FBerlin'
  D25='https://faser-grafana.web.cern.ch/render/d-solo/B84-cEBVz/late-trigger-fractions?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=3&width=1000&height=500&tz=Europe%2FBerlin'

  N=21
  auth-get-sso-cookie -u $D1 -o cookie.txt
  echo "Downloading TI12 1/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D1 > hv_voltage.png
  echo "Downloading TI12 2/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D2 > hv_current.png
  echo "Downloading TI12 3/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D3 > lv_idd.png
  echo "Downloading TI12 4/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D4 > lv_icc.png
  echo "Downloading TI12 5/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D5 > temperatures-SCT_A.png
  echo "Downloading TI12 6/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D6 > temperatures_SCT_B.png
  echo "Downloading TI12 7/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D7 > temperatures_frame.png
  echo "Downloading TI12 8/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D8 > humidity.png
  echo "Downloading TI12 9/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D9 > temperatures_TRB.png
  echo "Downloading TI12 10/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D10 > scintillator_timing.png
  echo "Downloading TI12 11/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D11 > scintillators_veto.png
  echo "Downloading TI12 12/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D12 > pdu_current.png
  echo "Downloading TI12 13/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D13 > pdu_loads.png
  echo "Downloading TI12 14/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D14 > rack_temperatures.png
  echo "Downloading TI12 15/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D15 > trigger_rate.png
  echo "Downloading TI12 16/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D16 > humidity_rack.png
  echo "Downloading TI12 17/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D17 > lhc_beam.png
  echo "Downloading TI12 18/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D18 > data_transfer_status.png
  echo "Downloading TI12 19/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D19 > eos_disk_usage.png
  echo "Downloading TI12 20/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D20 > recent_operations.png
  echo "Downloading TI12 21/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D21 > data_transfer_issues.png
  echo "Downloading TI12 22/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D22 > faser_nu_temperature.png
  echo "Downloading TI12 23/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D23 > veto_timing.png
  echo "Downloading TI12 24/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D24 > early_trigger.png
  echo "Downloading TI12 25/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D25 > late_trigger.png
  
  rm cookie.txt
}

download_EHN1 () {
  D1='https://faser-grafana.web.cern.ch/render/d-solo/myldGWW7k/ehn1-tracker-hv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FZurich'
  D2='https://faser-grafana.web.cern.ch/render/d-solo/pcakMZWnz/ehn1-tracker-lv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D3='https://faser-grafana.web.cern.ch/render/d-solo/pcakMZWnz/ehn1-tracker-lv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FZurich'
  D4='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FZurich'
  D5='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FZurich'
  D6='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FZurich'
  D7='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D8='https://faser-grafana.web.cern.ch/render/d-solo/fs_rWWWnz/testbeam-scintillators-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D9='https://faser-grafana.web.cern.ch/render/d-solo/r1RvM7W7k/testbeam-trigger?orgId=1&refresh=10s&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FZurich'
  
  N=9
  auth-get-sso-cookie -u $D1 -o cookie.txt
  echo "Downloading EHN1 1/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D1 > hv_voltage.png
  echo "Downloading EHN1 2/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D2 > lv_digital.png
  echo "Downloading EHN1 3/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D3 > lv_analog.png
  echo "Downloading EHN1 4/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D4 > temperatures-NtcA.png
  echo "Downloading EHN1 5/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D5 > temperatures_NtcB.png
  echo "Downloading EHN1 6/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D6 > temperatures_frame.png
  echo "Downloading EHN1 7/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D7 > humidity.png
  echo "Downloading EHN1 8/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D8 > calorimeter.png
  echo "Downloading EHN1 9/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D9 > trigger_rate.png

  rm cookie.txt
}

make_presentation () {
  #echo 'Producing template for the presentation.'

  if ! [ -d $PRESENTATION ]; then
    mkdir $PRESENTATION
    cd $PRESENTATION
    mkdir figures

    cp ../.tex/presentation.tex ./
    cp -r ../${DIR}/TI12/* ./figures

    sed -i "s/@from/${DATE_FROM}/g" presentation.tex
    sed -i "s/@to/${DATE_TO}/g" presentation.tex
  else
    cd $PRESENTATION
  fi
  
  #Deleting old version of labels
  sed -i '/\\vspace{-1.5em} %/d' presentation.tex
  sed -i '/\\begin{multicols}{2} %/d' presentation.tex
  sed -i '/\\setlength\\itemsep{0.005em} %/d' presentation.tex
  sed -i '/\\draw/d' presentation.tex
  sed -i '/\\bulletpoint{/d' presentation.tex
  sed -i '/\\tiny{\\begin{itemize} %/d' presentation.tex
  sed -i '/\\end{itemize}} %/d' presentation.tex
  sed -i '/\\end{multicols} %/d' presentation.tex

  #echo "Putting labels to the presentation."
  counter=0
  while read -r LINE 
  do
    if ! [[ $LINE == \#* ]] && ! [[ $LINE =~ ^[[:space:]]*$ ]]; then
      DATE=`echo $LINE | cut -d " " -f 1`
      TIME=`echo $LINE | cut -d " " -f 2`
      #LABEL=`echo $LINE | cut -d " " -f 3-`
      LABEL=`echo $LINE`
      UNIX_DATE=`date --date=${DATE}' '${TIME}' +0000' +%s`
      if [[ "${UNIX_DATE}" -gt "${TO}"  ||  "${UNIX_DATE}" -lt "${FROM}" ]]; then
        echo "WARNING: Label \"(${LABEL})\" in the plot is out of time range. Skipping this one."
        continue 
      fi
      if [[ $counter == 0 ]]; then
        sed -i "/^.*@item/i \\\\\\vspace{-1.5em} %" presentation.tex
        sed -i "/^.*@item/i \\\\\\begin{multicols}{2} %" presentation.tex
        sed -i "/^.*@item/i \\\\\\tiny{\\\\\\begin{itemize} %" presentation.tex
        sed -i "/^.*@item/i \\\\\\setlength\\\\\\itemsep{0.05em} %" presentation.tex
        sed -i "/^.*@item/a \\\\\\end{multicols} %" presentation.tex
        sed -i "/^.*@item/a \\\\\\end{itemize}} %" presentation.tex
      fi
      counter=$((counter+1))
      LATEX_POS_L=0.45
      LATEX_POS_R=11.6
      LATEX_POS_OFFSET=0.45
      LATEX_POS=`python -c "print( ($UNIX_DATE - $FROM) / float($TO - $FROM) * ($LATEX_POS_R - $LATEX_POS_L) + $LATEX_POS_OFFSET )"`
      NODE="\\\\\\draw[latex-,green] (${LATEX_POS},5) -- ++ (0,0.5) node[above,black,fill=white]{\\\\\\small ${counter}};"
      ITEM="\\\\\\bulletpoint{${counter})} ${LABEL}"
      sed -i "/^.*@node/i ${NODE}" presentation.tex
      sed -i "/^.*@item/i ${ITEM}" presentation.tex
    fi
  done < "../config.txt"

  pdflatex presentation.tex 2>&1 > /dev/null
  rm presentation.log
  rm presentation.nav
  rm presentation.out
  rm presentation.aux
  rm presentation.snm
  rm presentation.toc
  rm presentation.vrb
 
  cd ../
  USER=`whoami`
  echo "Presentation template is produced in folder ${PRESENTATION}"
  echo "For downloading (secure copy) TI12 slides, run from your local PC following command:"
  echo "scp -r ${USER}@lxplus.cern.ch:${PWD}/${PRESENTATION} ./"
}

if ! [ "$6" == "--presentation" ]; then
  if [ ! -d "$DIR" ]; then
    mkdir $DIR
    cd $DIR
  else
    cd $DIR
  fi
  
  if [[ ${1} == "TI12" ]]; then
    if [ ! -d "./TI12" ]; then
      mkdir ./TI12
      cd ./TI12
      download_TI12
      cd ../../    
      make_presentation
    else
      echo "WARNING: Folder with name TI12 already exists! It means that you have probably"
      echo "already downloaded grafana dashbords from TI12 in given timerange."
      echo "If you want to continue, remove or rename folder $DIR/TI12."
      cd ../
      exit
    fi
  
  elif [[ ${1} == "EHN1" ]]; then
    if [ ! -d "./EHN1" ]; then
      mkdir ./EHN1
      cd ./EHN1
      download_EHN1
      cd ../../
    else
      echo "WARNING: Folder with name EHN1 already exists! It means that you have probably"
      echo "already downloaded grafana dashbords from EHN1 in given timerange."
      echo "If you want to continue, remove or rename folder $DIR/EHN1."
      cd ../
      exit
    fi
  
  elif [[ ${1} == "both" ]]; then
    if [ ! -d "./TI12" ]; then
      mkdir ./TI12
      cd ./TI12
      download_TI12
      cd ../../
      make_presentation
      cd $DIR
    else
      echo "WARNING: Folder with name TI12 already exists! It means that you have probably"
      echo "already downloaded grafana dashbords from TI12 in given timerange."
      echo "If you want to continue, remove or rename folder $DIR/TI12."
      cd ../
      exit
    fi
  
    if [ ! -d "./EHN1" ]; then
      mkdir ./EHN1
      cd ./EHN1
      download_EHN1
      cd ../../
    else
      echo "WARNING: Folder with name EHN1 already exists! It means that you have probably"
      echo "already downloaded grafana dashbords from EHN1 in given timerange."
      echo "If you want to continue, remove or rename folder $DIR/EHN1."
      cd ../
      exit
    fi
  
  else
    echo "WARNING: Unknown option."
    print_help
  fi
  
  echo 'Downloading finished!' 
fi

if [ "$6" == "--presentation" ]; then
  if [ -d "${DIR}/TI12" ]; then
    make_presentation
  else
    echo "ERROR: You selected to create presentation for the time range for which no TI12 data is available."
    echo "Solution: Run the same command but without flag --presentation using option 'both' or 'TI12'."
  fi
fi
