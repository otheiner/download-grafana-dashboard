What is this script used for?  
------------------------------------ 
This is the script that downloads Grafana monitoring dashboards from FASER experiment. It also creates presentation for weekly shifter report for data from TI12. It also allows batch addition of labels to all downloaded Grafana plots using one simple configuration file. The script creates two folders. One for downloaded plots and the other one for slides. Name of the folder uses start and end date in format of UNIX timestamp (number of seconds since 1970-01-01 00:00:00 UTC). Downloading part has to be used on lxplus otherwise it will not work because it uses Kerberos authentication (you need to have valid Kerberos ticket). Presentation-making part can run on any Linux machine with `pdflatex` installed.

If you have any comments or you want to add some additional plots, just let me know, or you can also do modifications yourself.

How to use the script?   
------------------------------------
<details>
  <summary>Expand to read more</summary>

Clone this repository anywhere to lxplus and go to the folder of the repository. This you do by following commands:

<pre>
git clone https://gitlab.cern.ch/otheiner/download-grafana-dashboard.git
cd download-grafana-dashboard
</pre> 

Then you can use the script. It downloads Grafana plots and creates slides. 

Usage: `./download-dashboard.sh <type of plots>  <start date> <end date>`  
`<type of plots>` can be one of three options (case sensitivie): 
  - EHN1 - downloads plots from EHN1
  - TI12 - downloads plots from TI12
  - both - downloads plots both from EHN1 and TI12

Date is in format YYYY-MM-DD hh:mm:ss. Time is in UTC. Example:   

<pre>
./download-dashboard.sh TI12 2022-05-04 12:00:00 2022-05-06 12:00:00
</pre>

In case you run the script with, let's say, TI12 option for a given time range, the script creates folder called `grafana_<UNIX_start_date>_<UNIX_end_date>` and inside it, there will be folder TI12 with TI12 plots. In case `TI12` or `both` options are sellected it also creates folder `slides_TI12_<UNIX_start_date>_<UNIX_end_date>` with the presentation. For more information about slides refer to sections bellow.
</details>

Adding labels to all plots in the presentation 
------------------------------------  
<details>
  <summary>Expand to read more</summary>

In case there is some intervention in the tunnel, we usually want to mark this event in Grafana plot and we want to add label stating what type of intervention it was. This is possible using configuration file `config.txt` in this repository. More details are written directly in the configuration file but for adding label `This is the amazing label` corresponding to time, let's say, `2021-05-04 17:00:00` (times are in UTC, Geneva is +1 hour) we only need to add following line to the `config.txt`:   

<pre>
2021-05-04 17:00:00 This is the amazing label
</pre>

This label is applied in all Grafana plots in the presentation. Lines starting with `#` in the configuration file are considered to be comment. Labels outside of the time range for which Grafana plots were downloaded are ignored. The example of such slides with labels is shown in figures bellow. Note that lines don't point exactly to the given moment, but only to the approximate point. If you think this is the issue, it can be fixed but it would be a lot of hassle considering that we use these labels only for the easier orientation in plots and that before they were drawn by hand (also probably not very precisely).

![Screenshot_2022-05-16_at_12.35.35](/uploads/9c4f8ebf61735e64082fb11fe1761e26/Screenshot_2022-05-16_at_12.35.35.png)
![Screenshot_2022-05-16_at_12.35.58](/uploads/9f98de210d8f803e57b21794ecfcaf54/Screenshot_2022-05-16_at_12.35.58.png)
</details>

Recreating presentation with already downloaded data 
------------------------------------ 
<details>
  <summary>Expand to read more</summary>
  
If Grafana folder exists for the given time range, it is possible to recreate only the presentation (namely adding new labels to plots, without need of running downloading part again). This runs in a few seconds on contrary to downloading stage which can take up to minute(s). Recreating presentation for the given time range requires running the same command as for downloading plus adding flag `--presentation` as the last argument. Following command would take downloaded data for time range `2021-05-04 12:00:00 2021-05-06 12:00:00` and only recreated presentation. Presentation is only updated, which means that labels are changed but rest of the presentation should be unchanged.  

<pre>
./download-dashboard.sh both 2022-05-04 12:00:00 2022-05-06 12:00:00 --presentation
</pre>

This command also automatically compiles presentation to `pdf`. However, if LaTeX encounters some problems in the syntax it won't compile presentation correctly. In case the presentation is not created, I recommend compiling LaTex file manually (in terminal or using your favourite LaTex editor).
</details>


How to compile LaTeX file with the presentation?
------------------------------------
<details>
  <summary>Expand to read more</summary>

If plots for TI12 are downloaded, LaTeX template for the final shifter report is created. Shifter then only needs to complete the presentation and compile it to pdf. Things that need to be completed by shifter in the presentation are marked by TODO label (there are 3 TODOs in the presentation). This can be done even on local PC after downloading the folder with slides (it contains presentation template and figures). Compilation can be done in two ways. First option is simply running this script (it is sufficient only with flag `--presentation`). The second option, in case something goes wrong in LaTeX, you can use some LaTeX editor or in command line by running `pdflatex` from inside folder with slides. LaTeX prints a lot of output in the terminal even if there is no error, so don't get confused by that.

<pre>
cd slides_*
pdflatex presentation.tex
</pre>
</details>

    

